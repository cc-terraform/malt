# Malt - Manage Access for Laptops with Terraform

Convenience script for updating firewalls to add, remove or update remote
access IPs for users who travel, work in libraries and coffee shops, or just
have a dynamic IPs wherever they are.

Currently only works with OpenStack.  (Contributions welcome!)

## Usage

The user must use the `clouds.yaml` method of OpenStack authentication as
opposed to the older `openrc` method.  See below for further information on
the use of this file.

```
$ export OS_CLOUD=<cloud_name>
$ malt
# [ ... does its thing ... ]
# [ ... you do your thing ... ]
$ malt --disable
```

It is good practice to run `malt --disable` when use of the current access
will no longer be necessary.

### `clouds.yaml`

On current versions of OpenStack a
prepopulated version of this file can be downloaded from Horizon under **API
Access** by choosing "OpenStack clouds.yaml File" from the "Download OpenStack
RC File" dropdown at the right-hand side.  (For access to multiple clouds and
OpenStack projects, multiple `clouds.yaml` files can be combined such that the
cloud names are unique.)

## How it works

On first run, Malt creates a directory `$HOME/.config/malt` where it deposits
the main Terraform configuration which is shared among the various OpenStack
clouds that may be used.

Terraform state is stored in `$HOME/config/malt/$OS_CLOUD`.  If this directory
does not exist it will be created.

There is one Terraform variable, `enable` which defaults to _true_.  If the
`--disable` switch is provided, this variable is set to _false_ which sets the
number of desired security group rules to zero--in essence removing the
existing rule and the access.

## Limitations and future work

* Only works on OpenStack.
* Only updates the "default" security group.  This is appropriate for my
  context and ensures the rule applies to all VMs in the project, but this may
  not be suitable in all cases.
